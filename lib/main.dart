import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testtask/view/list_goods/list_goods_provider.dart';
import 'package:testtask/view/log_in/log_in_provider.dart';
import 'package:testtask/view/log_in/log_in.dart';
import 'package:testtask/view/list_goods/list_goods.dart';
import 'package:testtask/providers/userStatus.dart';
import 'package:testtask/model/user.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthProvider()),
        StreamProvider<User>.value(
          value: UserStatus().currentUser,
        ),
      ],
      child: MaterialApp(
        theme: ThemeData(primarySwatch: Colors.blueGrey),
        debugShowCheckedModeBanner: false,
        home: MainScreen(),
      ),
    );
  }
}

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final User user = Provider.of<User>(context);
    final bool isLoggedIn  = user != null;
    return isLoggedIn
        ? ChangeNotifierProvider(
            create: (_) => ChangeViewProvider(),
            child: ListGoods(),
          )
        : ChangeNotifierProvider<AuthProvider>(
            create: (_) => AuthProvider(), child: LogIn());
  }
}
