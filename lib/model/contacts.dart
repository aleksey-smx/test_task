import 'package:cloud_firestore/cloud_firestore.dart';

class Contact {
  String id;
  String name;
  String image;

  Contact({this.id, this.name, this.image});

  factory Contact.fromFirebase(DocumentSnapshot snapshot) => Contact(
      id: snapshot.data['id'] as String,
      name: snapshot.data['name'] as String,
      image: snapshot.data['image'] as String);
}
