import 'package:cloud_firestore/cloud_firestore.dart';

class Good {
  String name;
  String price;
  String description;
  String image;

  Good({this.name, this.price, this.description, this.image});

  factory Good.fromJson(DocumentSnapshot snapshot) {
    return Good(
        name: snapshot['name'] as String,
        price: snapshot['price'].toString(),
        description: snapshot['description'] as String,
        image: snapshot['image'] as String);
  }
}
