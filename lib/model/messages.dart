import 'package:cloud_firestore/cloud_firestore.dart';

class Messages {
  String userFrom;
  String userTo;
  String message;
  String date;
  String type;

  Messages({this.userFrom, this.userTo, this.message, this.date, this.type});

  factory Messages.fromFirebase(DocumentSnapshot snapshot) {
    return Messages(
        userFrom: snapshot['from'] as String,
        userTo: snapshot['to'] as String,
        message: snapshot['msg'] as String,
        date: snapshot['date'] as String,
        type: snapshot['type'] as String);
  }

  Map<String, dynamic> toFirebase() => <String, dynamic>{
        'from': userFrom,
        'to': userTo,
        'msg': message,
        'date': date,
        'type': type
      };
}
