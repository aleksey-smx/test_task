import 'package:firebase_auth/firebase_auth.dart';

class User {
  String id;
  String mail;
  String name;
  String image;

  User({this.id, this.mail, this.name, this.image});

  factory User.fromFirebase(FirebaseUser user) {
    return User(
        id: user.uid,
        mail: user.email,
        name: user.displayName == null ? user.email : user.displayName,
        image: user.photoUrl == null
            ? 'https://colewest.com/wp-content/uploads/2016/12/user-placeholder.jpg'
            : user.photoUrl);
  }

  Map<String, dynamic> toFirebase() =>
      <String, dynamic>{'id': id, 'name': name, 'mail': mail, 'image': image};
}
