import 'package:firebase_auth/firebase_auth.dart';
import 'package:testtask/model/user.dart';

// TODO: Сделаю стрим-провайдером
class UserStatus {
  final FirebaseAuth _fAuth = FirebaseAuth.instance;

  Stream<User> get currentUser {
    return _fAuth.onAuthStateChanged.map(
        (FirebaseUser user) => user != null ? User.fromFirebase(user) : null);
  }
}
