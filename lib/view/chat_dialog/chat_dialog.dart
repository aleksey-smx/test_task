import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testtask/model/contacts.dart';
import 'package:testtask/model/messages.dart';
import 'package:testtask/model/user.dart';
import 'package:testtask/view/chat_dialog/chat_dialog_provider.dart';
import 'package:image_picker/image_picker.dart';

class Chat extends StatefulWidget {
  final Contact contact;

  Chat({Key key, @required this.contact}) : super(key: key);

  @override
  _ChatState createState() => _ChatState(contact);
}

class _ChatState extends State<Chat> {
  String _message;
  String _userFrom;
  String _userTo;
  String _chatID;
  final _messageController = TextEditingController(text: '');
  ChatProvider _msg = ChatProvider();
  Contact contact;

  _ChatState(this.contact);

  @override
  void initState() {
    super.initState();
    loadMsg(_chatID);
  }

  Future loadMsg(String chatID) async {
    await _msg.setID(chatID);
  }

  @override
  Widget build(BuildContext context) {
    GlobalKey<FormState> _messageStateKey = GlobalKey<FormState>();

    User user = Provider.of(context);

    _userFrom = user.id;
    _userTo = contact.id;

    if (_userFrom.hashCode <= _userTo.hashCode)
      _chatID = '$_userFrom-$_userTo';
    else
      _chatID = '$_userTo-$_userFrom';

    return Scaffold(
      appBar: AppBar(
        title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(45),
                child: Image.network(
                  contact.image,
                  width: 45,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(child: Text(contact.name)),
            ]),
      ),
      body: ChangeNotifierProvider.value(
        value: ChatProvider(),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.black12,
            image: DecorationImage(
                image: AssetImage('assets/images/bg.jpg'), fit: BoxFit.cover),
          ),
          padding: EdgeInsets.only(bottom: 50.0, left: 12.0, right: 12.0),
          child: Consumer<ChatProvider>(builder: (context, messages, child) {
            messages.loadMessages(_chatID);
            if (messages.getMsg == null) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else if (messages.getMsg.isEmpty) {
              return noMessage();
            } else {
              return ListView.builder(
                  reverse: true,
                  itemCount: messages.getMsg.length,
                  itemBuilder: (context, index) {
                    return messages.getMsg[index].userFrom == user.id
                        ? Container(
                            margin: EdgeInsets.only(
                                left: 45.0, top: 6.0, bottom: 4.0),
                            padding: EdgeInsets.only(left: 4, bottom: 4),
                            decoration: BoxDecoration(
                                color: Colors.blue[100],
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10.0),
                                    topRight: Radius.circular(10.0),
                                    topLeft: Radius.circular(10.0))),
                            child: ListTile(
                                focusColor: Colors.blue,
                                title: messages.getMsg[index].type == '0'
                                    ? Text(
                                        messages.getMsg[index].message,
                                        textAlign: TextAlign.right,
                                      )
                                    : CachedNetworkImage(
                                        imageUrl:
                                            messages.getMsg[index].message,
                                        progressIndicatorBuilder: (context, url,
                                                downloadProgress) =>
                                            Center(
                                              child: CircularProgressIndicator(
                                                  value: downloadProgress
                                                      .progress),
                                            ),
                                        errorWidget: (context, url, error) =>
                                            Icon(Icons.error),
                                        fit: BoxFit.cover)),
                          )
                        : Container(
                            margin: EdgeInsets.only(
                                right: 45.0, top: 6.0, bottom: 4.0),
                            padding: EdgeInsets.all(2.0),
                            decoration: BoxDecoration(
                                color: Colors.blue[200],
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10.0),
                                    topRight: Radius.circular(10.0),
                                    topLeft: Radius.circular(10.0))),
                            child: ListTile(
                              title: messages.getMsg[index].type == '0'
                                  ? Text(
                                      messages.getMsg[index].message,
                                    )
                                  : CachedNetworkImage(
                                      imageUrl: messages.getMsg[index].message,
                                      progressIndicatorBuilder: (context, url,
                                              downloadProgress) =>
                                          Center(
                                            child: CircularProgressIndicator(
                                                value:
                                                    downloadProgress.progress),
                                          ),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                      fit: BoxFit.cover),
                            ),
                          );
                  });
            }
          }),
        ),
      ),
      bottomSheet: Container(
        color: Colors.white,
        height: 50,
        padding: EdgeInsets.only(left: 8.0),
        child: Form(
          key: _messageStateKey,
          child: TextFormField(
            controller: _messageController,
            maxLines: 20,
            decoration: InputDecoration(
              prefixIcon: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                      icon: Icon(Icons.image),
                      onPressed: () => _msg.sendPhoto(
                          Messages(
                              userTo: _userTo,
                              userFrom: _userFrom,
                              date: DateTime.now()
                                  .microsecondsSinceEpoch
                                  .toString(),
                              type: '1'),
                          ImageSource.gallery)),
                  IconButton(
                      icon: Icon(Icons.camera_alt),
                      onPressed: () => _msg.sendPhoto(
                          Messages(
                              userTo: _userTo,
                              userFrom: _userFrom,
                              date: DateTime.now()
                                  .microsecondsSinceEpoch
                                  .toString(),
                              type: '1'),
                          ImageSource.camera)),
                ],
              ),
              suffixIcon: IconButton(
                  icon: Icon(
                    Icons.send,
                    color: Colors.blueGrey,
                  ),
                  onPressed: () => sendMessage()),
              border: InputBorder.none,
              hintText: "Enter your message",
            ),
          ),
        ),
      ),
    );
  }

  Center noMessage() {
    return Center(
      child: Container(
        padding: EdgeInsets.all(22),
        decoration: BoxDecoration(
          color: Colors.black12,
          borderRadius: BorderRadius.circular(80.0),
        ),
        width: 200,
        height: 200,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              width: 145,
              child: Text(
                'There are no messages yet',
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 22,
                    color: Colors.black38),
              ),
            ),
            Icon(
              Icons.send,
              color: Colors.black38,
              size: 34,
            )
          ],
        ),
      ),
    );
  }

  sendMessage() {
    _message = _messageController.text.trim();
    if (_message.isEmpty) return;

    Messages data = Messages(
        userTo: _userTo,
        userFrom: _userFrom,
        message: _message,
        date: DateTime.now().microsecondsSinceEpoch.toString(),
        type: '0');
    _msg.sendMessage(data);
    _messageController.clear();
  }
}
