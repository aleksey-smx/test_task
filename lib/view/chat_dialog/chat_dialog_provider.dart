import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:testtask/model/messages.dart';
import 'package:image_picker/image_picker.dart';

class ChatProvider with ChangeNotifier {
  var _groupChatId;
  List<Messages> _messages;

  List<Messages> get getMsg => _messages;

  setID(String groupChatId) async {
    _groupChatId = groupChatId;
    await loadMessages(groupChatId);
    notifyListeners();
  }

  Future <List<Messages>>loadMessages(String chatID) async {
    _messages = await Firestore.instance
        .collection('messages')
        .document(chatID)
        .collection('chat')
        .orderBy('date', descending: true)
        .getDocuments()
        .then((snapshot) =>
            snapshot.documents.map((e) => Messages.fromFirebase(e)).toList());

    notifyListeners();
    return _messages;
  }

  sendMessage(Messages data) {
    if (data.userFrom.hashCode <= data.userTo.hashCode)
      _groupChatId = '${data.userFrom}-${data.userTo}';
    else
      _groupChatId = '${data.userTo}-${data.userFrom}';

    Firestore.instance
        .collection('messages')
        .document(_groupChatId)
        .collection('chat')
        .add(data.toFirebase());
  }

  sendPhoto(Messages data, ImageSource imageSource) async {
    final img = await ImagePicker().getImage(source: imageSource);

    String fileName = '${data.userFrom}-${data.date}.jpg';
    var setFile = FirebaseStorage.instance.ref().child(fileName);
    setFile
        .putFile(File(img.path))
        .onComplete
        .then((value) => value.ref.getDownloadURL().then((value) {
              data.message = value;
              sendMessage(data);
            }));
  }
}
