import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testtask/model/contacts.dart';
import 'package:testtask/model/user.dart';
import 'package:testtask/view/chat_dialog/chat_dialog.dart';
import 'package:testtask/view/chat_list/chat_list_provider.dart';

class ChatList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ChatListState();
}

class _ChatListState extends State<ChatList> {
  @override
  Widget build(BuildContext context) {
    final User user = Provider.of<User>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Contacts'),
        centerTitle: true,
      ),
      body: FutureProvider(
        create: (_) async => ChatListProvider().getContacts(),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.black12,
            image: DecorationImage(
                image: AssetImage('assets/images/messageBG.png'),
                fit: BoxFit.fill),
          ),
          child: Consumer<List<Contact>>(
              builder: (context, List<Contact> _contacts, _) {
            return _contacts == null
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView.builder(
                    itemCount: _contacts.length,
                    itemBuilder: (BuildContext context, index) {
                      var data = _contacts[index];
                      if (data.id == user.id) {
                        data.name = 'Saved messages';
                        data.image =
                            'https://static.socialkade.ir/media/telegram-channel/2/6390441316-2-Savedd_messages.jpg';
                      }
                      return Card(
                        child: Container(
                          padding: EdgeInsets.only(top: 6.0),
                          height: 70,
                          child: ListTile(
                            leading: ClipRRect(
                                borderRadius: BorderRadius.circular(45.0),
                                child: Image.network(data.image)),
                            title: Text(
                              data.name,
                              style: TextStyle(fontWeight: FontWeight.w300),
                            ),
                            trailing: IconButton(
                              icon: Icon(Icons.chat),
                              onPressed: () {
                                Contact _contact = Contact(
                                    id: data.id,
                                    name: data.name,
                                    image: data.image);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Chat(
                                              contact: _contact,
                                            )));
                              },
                            ),
                          ),
                        ),
                      );
                    });
          }),
        ),
      ),
    );
  }
}
