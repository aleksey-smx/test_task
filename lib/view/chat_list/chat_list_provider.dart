import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:testtask/model/contacts.dart';

class ChatListProvider {
  Future<List<Contact>> getContacts() async {
    List<Contact> data = await Firestore.instance
        .collection('users')
        .getDocuments()
        .then((snapshot) =>
            snapshot.documents.map((e) => Contact.fromFirebase(e)).toList());
    return data;
  }
}
