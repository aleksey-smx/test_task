import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testtask/model/user.dart';
import 'package:testtask/view/chat_list/chat_list.dart';
import 'package:testtask/view/log_in/log_in_provider.dart';

class DrawerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final TextStyle accInfo = TextStyle(color: Colors.white60, fontSize: 12.0);
    final AuthProvider _exit = Provider.of(context);
    final User user = Provider.of<User>(context);
    return Drawer(
      child: ListView(
        children: [
          Container(
            height: 130.0,
            child: DrawerHeader(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 90.0,
                    child: ClipOval(
                      child: CachedNetworkImage(
                        imageUrl: user.image,
                        progressIndicatorBuilder:
                            (context, url, downloadProgress) => Center(
                          child: CircularProgressIndicator(
                              value: downloadProgress.progress),
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 3.0,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.mail,
                            color: Colors.white70,
                            size: 14.0,
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          Text(
                            user.mail,
                            style: accInfo,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 2.0,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.person,
                            color: Colors.white70,
                            size: 14.0,
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          Text(
                            user.name,
                            style: accInfo,
                          )
                        ],
                      )
                    ],
                  )
                ],
              ),
              decoration: BoxDecoration(color: Colors.blueGrey),
            ),
          ),
          ListTile(
            leading: Icon(
              CupertinoIcons.clear_circled_solid,
              color: Colors.redAccent,
            ),
            title: Text('Log out'),
            onTap: () {
              _exit.logOut();
            },
          ),
          ListTile(
            leading: Icon(
              Icons.chat,
              color: Colors.blueGrey,
            ),
            title: Text('Chat'),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => ChatList()));
            },
          ),
        ],
      ),
    );
  }
}
