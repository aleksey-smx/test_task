import 'package:flutter/material.dart';
import 'package:testtask/view/email/email_provider.dart';
import 'package:testtask/model/user.dart';


class EmailLogIn extends StatefulWidget {
  @override
  _EmailLogInState createState() => _EmailLogInState();
}

class _EmailLogInState extends State<EmailLogIn> {
  String _email;
  String _password;

  final _emailController = TextEditingController(text: '');
  final _passwordController = TextEditingController(text: '');

  AuthEmail _authEmail = AuthEmail();

  @override
  Widget build(BuildContext context) {
    GlobalKey<FormState> _formStateKey = GlobalKey<FormState>();
    return Scaffold(
        body: Container(
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(400.0)),
            ),
            child: Center(
              child: Padding(
                padding: EdgeInsets.only(left: 60, right: 60),
                child: Container(
                  child: Wrap(
                      alignment: WrapAlignment.center,
                      spacing: 2.0,
                      runSpacing: 2.0,
                      children: [
                        SizedBox(
                          height: 100,
                          child: Image.asset('assets/images/cart.png'),
                        ),
                        Form(
                            key: _formStateKey,
                            autovalidate: true,
                            child: Column(
                              children: [
                                TextFormField(
                                  validator: validateEmail,
                                  onSaved: (String val) {
                                    _email = val;
                                  },
                                  style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontWeight: FontWeight.bold),
                                  controller: _emailController,
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                      icon: Icon(Icons.email),
                                      border: OutlineInputBorder(),
                                      labelText: 'E-mail'),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                TextField(
                                  obscureText: true,
                                  style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontWeight: FontWeight.bold),
                                  controller: _passwordController,
                                  decoration: InputDecoration(
                                    icon: Icon(Icons.lock),
                                    border: OutlineInputBorder(),
                                    labelText: 'Password',
                                  ),
                                ),
                              ],
                            )),
                        ButtonBarTheme(
                          data: ButtonBarThemeData(
                              alignment: MainAxisAlignment.center),
                          child: ButtonBar(
                            children: <Widget>[
                              FlatButton(
                                color: Colors.blueGrey,
                                child: Text(
                                  'LOGIN',
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white54,
                                      fontWeight: FontWeight.w400),
                                ),
                                onPressed: () {
                                  logInButton();
                                },
                              ),
                            ],
                          ),
                        ),
                      ]),
                ),
              ),
            )));
  }

  String validateEmail(String value) {
    Pattern emailValid =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    Pattern emojiValid =
        "(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])";

    if (RegExp(emojiValid).hasMatch(value)) return 'Emoji detected';
    if (!RegExp(emailValid).hasMatch(value)) return 'Enter Valid Email';

    if (value.isEmpty)
      return 'This field can\'t be empty';
    else
      return null;
  }

  void logInButton() async {
    _email = _emailController.text;
    _password = _passwordController.text;

    if (_email.isEmpty || _password.isEmpty) return;

    User user = await _authEmail.signIn(_email.trim(), _password.trim());
    if (user == null) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Can\'t log in. Please check your email and password'),
      ));
    } else {
      _emailController.clear();
      _passwordController.clear();
      Navigator.pop(context);
    }
  }

  void validatorEmail(value) {}


}
