import 'package:firebase_auth/firebase_auth.dart';
import 'package:testtask/model/user.dart';

class AuthEmail {
  final FirebaseAuth _fAuth = FirebaseAuth.instance;

  Future<User> signIn(String email, String password) async {
    AuthResult result = await _fAuth.signInWithEmailAndPassword(
        email: email, password: password);
    FirebaseUser user = result.user;

    return User.fromFirebase(user);
  }

  Future logOut() async {
    await _fAuth.signOut();
  }
}
