import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:testtask/model/goods.dart';

class GoodsDetail extends StatelessWidget {
  final Good goods;

  GoodsDetail({this.goods});

  @override
  Widget build(BuildContext context) {
    final TextStyle _description =
        TextStyle(fontSize: 20.0, fontWeight: FontWeight.w200);
    return Scaffold(
      appBar: AppBar(
        title: Text('About ${this.goods.name}'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.topLeft,
          child: Column(
            children: [
              Container(
                height: 400.0,
                child: CachedNetworkImage(
                  imageUrl: this.goods.image,
                  progressIndicatorBuilder: (context, url, downloadProgress) =>
                      Center(
                    child: CircularProgressIndicator(
                        value: downloadProgress.progress),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                  fit: BoxFit.fitWidth,
                ),
              ),
              Container(
                color: Colors.black12,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.all(12),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: Text(
                                'About this:',
                                style: TextStyle(
                                    fontSize: 32, fontWeight: FontWeight.w200),
                              ),
                            ),
                            Text(
                              this.goods.description,
                              style: _description,
                            )
                          ]),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              tooltip: 'Start chat',
              icon: Icon(Icons.chat),
              color: Colors.blueGrey,
              onPressed: () {
                Navigator.pushNamed(context, '/chatlist');
              },
            ),

            Text(
              '${this.goods.price} UAH',
              style: TextStyle(color: Colors.blueGrey, fontSize: 20.0),
            )
            //IconButton
          ],
        ),
      ),
    );
  }
}
