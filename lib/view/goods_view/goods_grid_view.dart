import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testtask/model/goods.dart';
import 'package:testtask/view/goods_detail/goods_detail.dart';

class GridGoods extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<List<Good>>(builder: (context, List<Good> value, _) {
      return value == null
          ? Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [CircularProgressIndicator(), Text('Loading..')],
            ))
          : GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 10.0,
                  mainAxisSpacing: 4.0),
              itemCount: value.length,
              itemBuilder: (BuildContext context, index) {
                var _goods = value[index];
                return SizedBox(
                  child: DecoratedBox(
                    decoration: BoxDecoration(
                        color: Colors.black12,
                        border: Border.all(width: 1.0, color: Colors.blueGrey)),
                    child: GestureDetector(
                      onTap: (() {
                        Good good = Good(
                            name: _goods.name,
                            price: _goods.price,
                            description: _goods.description,
                            image: _goods.image);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => GoodsDetail(
                                      goods: good,
                                    )));
                      }),
                      child: Container(
                        child: GridTile(
                          header: Container(
                            padding: EdgeInsets.only(
                                left: 12.0, top: 2.0, bottom: 2.0),
                            color: Colors.indigo,
                            child: Text(
                              _goods.name,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          footer: Container(
                              color: Colors.grey,
                              height: 40.0,
                              padding: EdgeInsets.only(
                                top: 4.0,
                                left: 8.0,
                              ),
                              child: Text(_goods.description.length >= 40
                                  ? _goods.description.substring(0, 40) + '..'
                                  : _goods.description)),
                          child: Container(
                              padding: EdgeInsets.all(16.0),
                              child: CachedNetworkImage(
                                  imageUrl: _goods.image,
                                  progressIndicatorBuilder: (context, url,
                                          downloadProgress) =>
                                      Center(
                                        child: CircularProgressIndicator(
                                            value: downloadProgress.progress),
                                      ),
                                  errorWidget: (context, url, error) =>
                                      Icon(Icons.error),
                                  fit: BoxFit.cover)),
                        ),
                      ),
                    ),
                  ),
                );
                //
              });
    });
  }
}
