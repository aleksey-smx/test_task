import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testtask/model/goods.dart';
import 'package:testtask/view/goods_detail/goods_detail.dart';

class GoodsListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<List<Good>>(
      builder: (context, List<Good> value, _) {
        return value == null
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: value.length,
                itemBuilder: (context, int index) {
                  var _goods = value[index];
                  return Card(
                    child: ListTile(
                      isThreeLine: true,
                      leading: DecoratedBox(
                        decoration: BoxDecoration(
                            border:
                                Border.all(color: Colors.blueGrey, width: 1.0)),
                        child: Container(
                          width: 80.0,
                          height: 60.0,
                          padding: EdgeInsets.all(1.0),
                          child: CachedNetworkImage(
                              imageUrl: _goods.image,
                              progressIndicatorBuilder:
                                  (context, url, downloadProgress) => Center(
                                        child: CircularProgressIndicator(
                                            value: downloadProgress.progress),
                                      ),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                              fit: BoxFit.cover),
                        ),
                      ),
                      title: Text(_goods.name),
                      subtitle: Text(_goods.description.length >= 50
                          ? _goods.description.substring(0, 50) + '..'
                          : _goods.description),
                      trailing: IconButton(
                          icon: Icon(
                            Icons.info,
                            color: Colors.blueGrey,
                          ),
                          tooltip: 'Detailed info',
                          onPressed: () {
                            Good good = Good(
                                name: _goods.name,
                                price: _goods.price,
                                description: _goods.description,
                                image: _goods.image);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GoodsDetail(
                                          goods: good,
                                        )));
                          }),
                    ),
                  );
                });
      },
    );
  }
}
