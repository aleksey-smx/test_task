import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:testtask/model/goods.dart';

class GoodsListProvider {
  Future<List<Good>> getGoodsFromQuery() async {
    List<Good> _goods = await Firestore.instance
        .collection('goods')
        .getDocuments()
        .then((snapshot) =>
            snapshot.documents.map((e) => Good.fromJson(e)).toList());
    return _goods;
  }
}
