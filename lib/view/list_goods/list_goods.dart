import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testtask/view/drawer/drawer.dart';
import 'package:testtask/view/goods_view/goods_grid_view.dart';
import 'package:testtask/view/goods_view/goods_list_view.dart';
import 'package:testtask/view/goods_view/provider_goods_view.dart';
import 'package:testtask/view/list_goods/list_goods_provider.dart';

class ListGoods extends StatefulWidget {
  @override
  _ListGoodsState createState() => _ListGoodsState();
}

class _ListGoodsState extends State<ListGoods> {
  @override
  Widget build(BuildContext context) {
    final _provider = Provider.of<ChangeViewProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Mobile Store'),
        centerTitle: true,
        actions: [
          IconButton(
              icon: _provider.grid ? Icon(Icons.list) : Icon(Icons.grid_on),
              tooltip: _provider.grid
                  ? 'Change view to ListView'
                  : ' Change view to GridView',
              onPressed: () {
                _provider.changeView();
              }),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(6.0),
        child: FutureProvider(
          create: (_) async => GoodsListProvider().getGoodsFromQuery(),
          child: _provider.grid ? GridGoods() : GoodsListView(),
        ),
      ),
      drawer: DrawerWidget(),
    );
  }
}
