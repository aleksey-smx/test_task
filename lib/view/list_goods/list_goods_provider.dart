import 'package:flutter/material.dart';

class ChangeViewProvider with ChangeNotifier {
  bool _grid = false;

  bool get grid => _grid;

  void changeView() {
    _grid = !_grid;
    notifyListeners();
  }
}
