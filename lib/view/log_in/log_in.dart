import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:provider/provider.dart';
import 'package:testtask/view/Email/Email.dart';
import 'package:testtask/view/log_in/log_in_provider.dart';
import 'package:testtask/view/register/register.dart';

class LogIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AuthProvider _gProvider = Provider.of(context);

    return ChangeNotifierProvider(
      create: (_) => AuthProvider(),
      child: Scaffold(
          body: Builder(
        builder: (context) => Container(
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(400.0)),
            ),
            child: Center(
              child: Padding(
                padding: EdgeInsets.only(left: 60, right: 60),
                child: Container(
                    child: Wrap(
                  runSpacing: 10.0,
                  alignment: WrapAlignment.center,
                  children: [
                    SizedBox(
                      height: 100,
                      child: Image.asset('assets/images/cart.png'),
                    ),
                    SignInButton(Buttons.Email, onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EmailLogIn()));
                    }),
                    SignInButton(Buttons.Google,
                        onPressed: () => _gProvider.signInGoogle()),
                    SignInButton(
                      Buttons.Facebook,
                      onPressed: () => _gProvider.signInFacebook(),
                    ),
                    GestureDetector(
                      child: Container(child: Text('New member? Register.', style: TextStyle(color: Colors.white),),),
                    onTap: ()=> Navigator.push(context, MaterialPageRoute(builder: (context) => Register())),)
                  ],
                )),
              ),
            )),
      )),
    );
  }
}
