import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:testtask/model/user.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class AuthProvider with ChangeNotifier {
  final FirebaseAuth _fAuth = FirebaseAuth.instance;
  final GoogleSignIn _gAuth = GoogleSignIn();
  final FacebookLogin _fbLogin = FacebookLogin();

  Future<User> signInGoogle() async {
    GoogleSignInAccount googleSignInAccount = await _gAuth.signIn();
    GoogleSignInAuthentication googleSignInAuth =
        await googleSignInAccount.authentication;
    AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: googleSignInAuth.idToken,
        accessToken: googleSignInAuth.accessToken);

    AuthResult result = await _fAuth.signInWithCredential(credential);
    final FirebaseUser user = result.user;
    var data = User.fromFirebase(user);
    Firestore.instance
        .collection('users')
        .document(data.id)
        .setData(data.toFirebase());

    return data;
  }

  Future<User> signInFacebook() async {
    var data;
    final FacebookLoginResult result = await _fbLogin.logIn([
      'email',
      'public_profile',
    ]);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        {
          var credential = FacebookAuthProvider.getCredential(
              accessToken: result.accessToken.token);
          AuthResult auth = await _fAuth.signInWithCredential(credential);
          final FirebaseUser user = auth.user;
          data = User.fromFirebase(user);
          Firestore.instance
              .collection('users')
              .document(data.id)
              .setData(data.toFirebase());
          break;
        }
      case FacebookLoginStatus.cancelledByUser:
        break;
      case FacebookLoginStatus.error:
        break;
    }
    return data;
  }

  logOut() async {
    _fAuth.signOut();
    await _gAuth.signOut();
  }
}
