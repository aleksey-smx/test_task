import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:testtask/model/user.dart';
import 'package:testtask/view/register/register_provider.dart';
import 'package:toast/toast.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  String _email;
  String _password;
  String _confirmPass;
  String _name;

  RegisterEmail _register = RegisterEmail();

  final _emailController = TextEditingController(text: '');
  final _passwordController = TextEditingController(text: '');
  final _confirmPasswordController = TextEditingController(text: '');
  final _nameController = TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    GlobalKey<FormState> _registerKey = GlobalKey<FormState>();
    return ChangeNotifierProvider.value(
        value: RegisterEmail(),
        child: Scaffold(
            body: Container(
                decoration: BoxDecoration(
                  color: Colors.grey,
                  borderRadius:
                      BorderRadius.only(bottomRight: Radius.circular(400.0)),
                ),
                child: Center(
                  child: Padding(
                    padding: EdgeInsets.only(left: 60, right: 60),
                    child: Container(
                      child: Wrap(
                          alignment: WrapAlignment.center,
                          spacing: 2.0,
                          runSpacing: 2.0,
                          children: [
                            Consumer<RegisterEmail>(
                                builder: (context, val, child) {
                              return GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  child: CircleAvatar(
                                    radius: 60,
                                    backgroundImage: val.image == null
                                        ? AssetImage(
                                            'assets/images/user-placeholder.jpg')
                                        : FileImage(val.image),
                                  ),
                                  onTap: () async {
                                    final imagePicker = ImagePicker();
                                    final PickedFile _pickedImage =
                                        await imagePicker.getImage(
                                            source: ImageSource.gallery);
                                    val.setImg(File(_pickedImage.path));
                                  });
                            }),
                            SizedBox(
                              height: 110.0,
                            ),
                            Form(
                                key: _registerKey,
                                autovalidate: true,
                                child: Column(
                                  children: [
                                    TextFormField(
                                      controller: _nameController,
                                      style: TextStyle(
                                          color: Colors.blueGrey,
                                          fontWeight: FontWeight.bold),
                                      decoration: InputDecoration(
                                          icon: Icon(
                                              Icons.supervised_user_circle),
                                          border: OutlineInputBorder(),
                                          labelText: 'Name'),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    TextFormField(
                                      controller: _emailController,
                                      validator: validateEmail,
                                      onSaved: (String val) {
                                        _email = val;
                                      },
                                      style: TextStyle(
                                          color: Colors.blueGrey,
                                          fontWeight: FontWeight.bold),
                                      decoration: InputDecoration(
                                          icon: Icon(Icons.email),
                                          border: OutlineInputBorder(),
                                          labelText: 'E-mail'),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    TextField(
                                      controller: _passwordController,
                                      obscureText: true,
                                      style: TextStyle(
                                          color: Colors.blueGrey,
                                          fontWeight: FontWeight.bold),
                                      decoration: InputDecoration(
                                        icon: Icon(Icons.lock),
                                        border: OutlineInputBorder(),
                                        labelText: 'Password',
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    TextField(
                                      controller: _confirmPasswordController,
                                      obscureText: true,
                                      style: TextStyle(
                                          color: Colors.blueGrey,
                                          fontWeight: FontWeight.bold),
                                      decoration: InputDecoration(
                                        icon: Icon(Icons.lock),
                                        border: OutlineInputBorder(),
                                        labelText: 'Confirm password',
                                      ),
                                    ),
                                  ],
                                )),
                            ButtonBarTheme(
                              data: ButtonBarThemeData(
                                  alignment: MainAxisAlignment.center),
                              child: ButtonBar(
                                children: <Widget>[
                                  FlatButton(
                                    color: Colors.blueGrey,
                                    child: Text(
                                      'REGISTER',
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.white54,
                                          fontWeight: FontWeight.w400),
                                    ),
                                    onPressed: () => registerButton(),
                                  ),
                                ],
                              ),
                            ),
                          ]),
                    ),
                  ),
                ))));
  }

  String validateEmail(String value) {
    Pattern emailValid =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    Pattern emojiValid =
        "(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])";

    if (RegExp(emojiValid).hasMatch(value)) return 'Emoji detected';
    if (!RegExp(emailValid).hasMatch(value)) return 'Enter Valid Email';

    if (value.isEmpty)
      return 'This field can\'t be empty';
    else
      return null;
  }

  void registerButton() async {
    _name = _nameController.text;
    _email = _emailController.text;
    _password = _passwordController.text;
    _confirmPass = _confirmPasswordController.text;

    if (_email.isEmpty || _password.isEmpty || _name.isEmpty) return;
    if (_password != _confirmPass) {
      Toast.show('Password does not match', context,
          duration: Toast.LENGTH_LONG,
          gravity: Toast.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white);
      return;
    }

    User user = await _register.registerUser(
        _email.trim(), _password.trim(), _name.trim());
    if (user == null) {
      Toast.show('', context,
          duration: Toast.LENGTH_LONG,
          gravity: Toast.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white);
    } else {
      _emailController.clear();
      _passwordController.clear();
      _nameController.clear();
      _confirmPasswordController.clear();
      _nameController.clear();
      Navigator.pop(context);
    }
  }
}
