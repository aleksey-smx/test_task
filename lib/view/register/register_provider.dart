import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:testtask/model/user.dart';
import 'dart:io';

class RegisterEmail with ChangeNotifier {
  final FirebaseAuth _fAuth = FirebaseAuth.instance;
  File _file;

  File get image => _file;

  setImg(var img) {
    _file = img;
    notifyListeners();
  }

  Future<User> registerUser(String email, String password, String name) async {
    AuthResult result = await _fAuth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );
    FirebaseUser user = result.user;
    var data = User.fromFirebase(user);
    data.name = name;
    Firestore.instance.collection('users').add(data.toFirebase());
    return data;
  }
}
